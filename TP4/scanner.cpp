#include "scanner.h"
#include <iostream>
#include <sstream>
#include "invalidcharexception.h"
using namespace std;

std::string Scanner::scan(File &file)
{
    size_t i = 0;

    for(auto &a : m_automates) {
        string token = a->match(file);
        if (!token.empty())
            return a->token() == " " ? "" : a->token();
        i++;
    }

    // char not matched by any automata
    if (i == m_automates.size() && !file.eof()) {
        stringstream msgBuilder;
        msgBuilder << "Invalid " << file.get() << ", at line" << file.line();
        throw InvalidCharException(msgBuilder.str());
    }

    return "";
}

void Scanner::scanFile(File &file, ostream &out)
{
    while(!file.eof()) {
        try {
            string token = scan(file);
            if (!token.empty())
                out << token << endl;
        } catch (Exception &exception) {
            cerr << exception.coloredMessage() << endl;
        }
    }
}

void Scanner::print() const {
    for (auto &a : m_automates)
        cout << a->token() << endl;
}

void Scanner::add(Automate* &&aut)
{
    m_automates.push_back(unique_ptr<Automate>(aut));
}
