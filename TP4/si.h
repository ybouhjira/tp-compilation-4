#ifndef SI_H
#define SI_H

#include "automate.h"

class Si : public Automate {
public:
    void run(char input);
    bool onTerminal () const;
    std::string token() const;
};

#endif // SI_H
