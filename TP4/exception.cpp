#include "exception.h"
#include <string>

Exception::Exception(const std::string &message) : m_message(message)
{
}

std::string Exception::message() const
{
    return m_message;
}

std::string Exception::coloredMessage() const
{
    return std::string("\x1b[31m") + m_message + std::string("\x1b[0m");
}


