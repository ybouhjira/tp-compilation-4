#ifndef SINON_H
#define SINON_H

#include "automate.h"

class Sinon : public Automate {
public:
    void run(char input);
    bool onTerminal () const;
    std::string token() const;
};

#endif // SINON_H
