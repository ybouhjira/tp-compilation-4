#include "alors.h"
#include <cassert>

bool Alors::onTerminal() const
{
    return state() == 6;
}

std::string Alors::token() const
{
    return "alors";
}

void Alors::run(char input)
{
    Automate::run(input);
    switch(state())
    {
    case 1:
        if(input == 'a') goToState(state() + 1);
        break;

    case 2:
        if(input == 'l') goToState(state() + 1);
        break;

    case 3:
        if(input == 'o') goToState(state() + 1);
        break;

    case 4:
        if(input == 'r') goToState(state() + 1);
        break;

    case 5:
        if(input == 's') goToState(state() + 1);
        break;
    }
}
