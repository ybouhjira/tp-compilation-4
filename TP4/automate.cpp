#include "automate.h"
#include <iostream>

using namespace std;

bool Automate::hasMoved() const {
    return m_moved;
}

void Automate::run(char input) {
    (void) input;
    m_moved = false;
}

void Automate::goToState(int state) {
    m_state = state;
    m_moved = true;
}

int Automate::state() const {
    return m_state;
}

void Automate::putBack(File &file, string &s)
{
    while(!s.empty()) {
        file << s.back();
        s.pop_back();
    }
}

string Automate::match(File &file)
{
    string accepted;
    string buffer;
    char c;

    do {
        file >> c;
        buffer += c;
        run(c);

        if(hasMoved() && onTerminal()) {
            accepted += buffer;
            buffer.clear();
        }

    } while(hasMoved() && !file.eof());

    putBack(file, buffer);
    reset();
    return accepted;
}

void Automate::reset()
{
    m_moved = false;
    m_state = 1;
}
