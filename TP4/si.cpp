#include <cassert>
#include "si.h"

void Si::run(char input) {
    Automate::run(input);

    switch (state()) {
    case 1:
        if (input == 's') goToState(state() + 1);
        break;
    case 2:
        if (input == 'i') goToState(state() + 1);
        break;
    }
}

bool Si::onTerminal () const {
    return state() == 3;
}

std::string Si::token() const {
    return "si";
}
