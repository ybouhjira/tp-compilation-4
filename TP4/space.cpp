#include "space.h"

bool Space::onTerminal() const
{
    return state() == 2;
}

std::string Space::token() const
{
    return " ";
}

void Space::run(char input)
{
    Automate::run(input);

    switch(state()) {
    case 1:
        if (isspace(input)) goToState(2);
        break;
    }
}
