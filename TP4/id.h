#ifndef ID_H
#define ID_H

#include "automate.h"

class Id : public Automate
{
public:


    // Automate interface
public:
    bool onTerminal() const;
    std::string token() const;
    void run(char input);
};

#endif // ID_H
