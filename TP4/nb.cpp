#include "nb.h"
#include <cassert>

bool Nb::onTerminal() const
{
    int st = state();
    return st == 2 ||
           st == 4 ||
           st == 7 ;
}

std::string Nb::token() const
{
    return "nb";
}

void Nb::run(char input)
{
    Automate::run(input);

    switch (state()) {
    case 1:
        if (isdigit(input)) goToState(2);
        break;

    case 2:
        if (input == '.') goToState(3);
        else if (isdigit(input)) goToState(2);
        else if (input == 'E' || input == 'e') goToState(5);
        break;

    case 3:
        if (isdigit(input)) goToState(4);
        break;

    case 4:
        if (isdigit(input)) goToState(4);
        else if (input == 'E' || input == 'e') goToState(5);
        break;

    case 5:
        if (input == '-' || input == '+') goToState(6);
        else if (isdigit(input)) goToState(7);
        break;

    case 6:
        if (isdigit(input)) goToState(7);
        break;

    case 7:
        if (isdigit(input)) goToState(7);
        break;
    }
}
