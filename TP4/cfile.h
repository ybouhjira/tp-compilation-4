#ifndef PROGRAMFILE_H
#define PROGRAMFILE_H

#include "file.h"
#include "fileopenexception.h"
#include <cstdio>

class CFile : public File
{
public:
    CFile(const std::string &filename);

    ~CFile();
    char get() override;
    bool eof() override;
    void putback(char c) override;

private:
    FILE *m_stream;
};

#endif // PROGRAMFILE_H
