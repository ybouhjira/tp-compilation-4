#ifndef NB_H
#define NB_H

#include "automate.h"

class Nb : public Automate
{
public:
    bool onTerminal() const;
    std::string token() const;
    void run(char input);
};

#endif // NB_H
