#ifndef FILEOPENEXCEPTION
#define FILEOPENEXCEPTION

#include "exception.h"

class FileOpenException : public Exception
{
public:
    using Exception::Exception;
};

#endif // FILEOPENEXCEPTION
