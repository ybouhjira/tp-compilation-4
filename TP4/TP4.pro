TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    automate.cpp \
    si.cpp \
    sinon.cpp \
    alors.cpp \
    oprel.cpp \
    id.cpp \
    file.cpp \
    cfile.cpp \
    nb.cpp \
    scanner.cpp \
    exception.cpp \
    space.cpp

HEADERS += \
    automate.h \
    si.h \
    sinon.h \
    alors.h \
    oprel.h \
    id.h \
    file.h \
    cfile.h \
    nb.h \
    scanner.h \
    invalidcharexception.h \
    exception.h \
    fileopenexception.h \
    space.h
