#include <iostream>
#include <memory>
#include <vector>
#include <exception>

#include "automate.h"
#include "alors.h"
#include "sinon.h"
#include "si.h"
#include "cfile.h"
#include "oprel.h"
#include "id.h"
#include "nb.h"
#include "space.h"

#include "scanner.h"

using namespace std;

int main()
{
    Scanner scanner;


    scanner.add(new Sinon);
    scanner.add(new Si);
    scanner.add(new Alors);
    scanner.add(new Oprel);
    scanner.add(new Nb);
    scanner.add(new Id);
    scanner.add(new Space);

    try {
        File &&file = CFile("program");
        scanner.scanFile(file, cout);

    } catch (Exception &exception) {
        cerr << exception.coloredMessage() << endl;
        return 1;
    }

    return 0;
}

