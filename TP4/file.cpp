#include "file.h"

void File::operator <<(char c)
{
    putback(c);
}

void File::operator>>(char &c)
{
    c = get();
}

void File::line(unsigned int line)
{
    m_line = line;
}

unsigned int File::line() const
{
    return m_line;
}
