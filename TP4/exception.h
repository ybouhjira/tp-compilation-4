#ifndef EXCEPTION_H
#define EXCEPTION_H

#include <stdexcept>

class Exception : public std::exception
{
public:
    Exception(const std::string &message);
    std::string message() const;
    std::string coloredMessage() const;

private:
    std::string m_message;
};

#endif // EXCEPTION_H
