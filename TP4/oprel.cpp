#include "oprel.h"
#include <cassert>

bool Oprel::onTerminal() const
{
    int st = state();
    return st == 2 ||
           st == 3 ||
           st == 4 ;
}

std::string Oprel::token() const
{
    return "oprel";
}

void Oprel::run(char input)
{
    Automate::run(input);

    switch (state()) {
    case 1:
        switch (input) {
        case '=':
            goToState(4);
            break;

        case '<':
            goToState(2);
            break;

        case '>':
            goToState(3);
            break;
        }
        break;

    case 2:
        switch (input) {
        case '=':
        case '>':
            goToState(4);
            break;
        }
        break;

    case 3:
        if (input == '=') goToState(4);
    }
}
