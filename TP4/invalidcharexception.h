#ifndef INVALIDCHAREXCEPTION_H
#define INVALIDCHAREXCEPTION_H

#include "exception.h"

class InvalidCharException : public Exception
{
public:
    using Exception::Exception;
};

#endif // INVALIDCHAREXCEPTION_H
