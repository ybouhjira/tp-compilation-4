#ifndef AUTOMATE_H
#define AUTOMATE_H

#include <string>
#include <utility>
#include <fstream>
#include <memory>
#include "file.h"

class Automate {
public:
    virtual bool onTerminal() const = 0;
    virtual std::string token() const = 0;

    virtual void run(char input);

    void reset();
    std::string match(File& file);
    bool hasMoved() const;

    virtual ~Automate() {}

protected:
    void goToState(int state);
    int state() const;

private:
    void putBack(File &file, std::string &s);

private:
    bool m_moved = false;
    int m_state = 1;
};

#endif // AUTOMATE_H
