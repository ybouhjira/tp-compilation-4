#include "sinon.h"

void Sinon::run(char input) {
    Automate::run(input);

    switch (state()) {
    case 1:
        if (input == 's') goToState(2);
        break;

    case 2:
        if (input == 'i') goToState(3);
        break;

    case 3:
        if (input == 'n') goToState(4);
        break;

    case 4:
        if (input == 'o') goToState(5);
        break;

    case 5:
        if (input == 'n') goToState(6);
        break;
    }
}

bool Sinon::onTerminal () const {
    return state() == 6;
}

std::string Sinon::token() const {
    return "sinon";
}
