#ifndef FILE_H
#define FILE_H

#include <string>

class File
{
public:
    virtual char get() = 0;
    virtual bool eof() = 0;
    virtual void putback(char c) = 0;

    virtual void operator<<(char c);
    virtual void operator>>(char &c);

    unsigned int line() const;

    ~File() {}

protected:
    void line(unsigned int line);

private:
    unsigned int m_line = 1;
};

#endif // FILE_H
