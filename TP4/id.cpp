#include "id.h"
#include <cassert>

bool Id::onTerminal() const
{
    return state() == 2;
}

std::string Id::token() const
{
    return "id";
}

void Id::run(char input)
{
    Automate::run(input);

    switch (state()) {
    case 1:
        if (isalpha(input)) goToState(2);
        break;

    case 2:
        if (isalpha(input)) goToState(2);
        else if (isdigit(input)) goToState(2);
        break;
    }
}
