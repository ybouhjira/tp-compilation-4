#include "cfile.h"
#include <cstdio>
#include <cctype>

using namespace std;

CFile::CFile(const string &filename)
{
    if(!(m_stream = fopen(filename.c_str(), "r"))) {
        throw FileOpenException("Unable to open file");
    }
}

CFile::~CFile()
{
    fclose(m_stream);
}

char CFile::get()
{
    char c = fgetc(m_stream);
    if (c == '\n') line(line() + 1);
    return c;
}

void CFile::putback(char c) {
    if (c == '\n') line(line() - 1);
    ungetc(c, m_stream);
}

bool CFile::eof()
{
    return feof(m_stream);
}

