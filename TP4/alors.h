#ifndef ALORS_H
#define ALORS_H

#include "automate.h"

class Alors : public Automate
{

public:
    bool onTerminal() const;
    std::string token() const;
    void run(char input);
};

#endif // ALORS_H
