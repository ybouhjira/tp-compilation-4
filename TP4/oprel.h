#ifndef OPREL_H
#define OPREL_H

#include "automate.h"

class Oprel : public Automate
{
public:

    // Automate interface
public:
    bool onTerminal() const;
    std::string token() const;
    void run(char input);
};

#endif // OPREL_H
