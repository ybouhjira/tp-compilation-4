#ifndef SPACE_H
#define SPACE_H

#include "automate.h"

class Space : public Automate
{
public:
    bool onTerminal() const;
    std::string token() const;
    void run(char input);
};

#endif // SPACE_H
