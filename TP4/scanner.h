#ifndef SCANNER_H
#define SCANNER_H

#include <vector>
#include <utility>
#include <memory>
#include <iostream>
#include "automate.h"

class Scanner
{
public:
    void add(Automate *&&ut);

    std::string scan(File &file);
    void scanFile(File &file, std::ostream &out);
    void print() const;

private:
    std::vector<std::unique_ptr<Automate>> m_automates;
};

#endif // SCANNER_H
